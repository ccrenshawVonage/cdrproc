package main

import (
	"encoding/json"
	"strings"
)

var fields = []string{
	"version", "environment", "cluster", "instance_id", "instance_ip", "track_id", "account_id", "service_ids",
	"caller_id_name", "caller_id_number", "dest_num", "context", "start_time", "answer_time", "end_time",
	"duration_sec", "billable_sec", "hangup_cause", "uuid", "bleg_uuid", "account_code", "read_codec", "write_codec",
	"originate_disp", "originating_leg_uuid", "sip_from_user", "sip_to_user", "duration_msec", "billable_msec",
	"sip_call_id", "feature_flags", "referred_by", "refer_to", "direction_a", "direction_b", "forwarded_by", "forwarded_by_exten",
	"src_line", "src_exten", "dst_line", "dst_exten", "orig_dest_num", "orig_caller_num", "greeting_phase",
	"primary_cust_tag", "secondary_cust_tag", "call_queue", "fax_document_transferred_pages",
}

// CDR is the raw cdr
type CDR struct {
	Version          int    `json:"version"`
	Environment      string `json:"environment"`
	Cluster          string `json:"cluster"`
	InstanceID       string `json:"instance_id"`
	InstanceIP       string `json:"instance_ip"`
	TrackID          string `json:"track_id"`
	AccountID        int    `json:"account_id"`
	ServiceIDs       string `json:"service_ids"`
	CallerIDName     string `json:"caller_id_name"`
	CallerIDNumber   string `json:"caller_id_number"`
	DestNum          string `json:"dest_num"`
	Context          string `json:"context"`
	StartTime        string `json:"start_time"`
	AnswerTime       string `json:"answer_time"`
	EndTime          string `json:"end_time"`
	DurationSec      int    `json:"duration_sec"`
	BillableSec      int    `json:"billable_sec"`
	HangupCause      string `json:"hangup_cause"`
	UUID             string `json:"uuid"`
	BLegUUID         string `json:"bleg_uuid"`
	AccountCode      string `json:"account_code"`
	ReadCodec        string `json:"read_codec"`
	WriteCodec       string `json:"write_codec"`
	OriginateDisp    string `json:"originate_disp"`
	OrigLegUUID      string `json:"originating_leg_uuid"`
	SipFromUser      string `json:"sip_from_user"`
	SipToUser        string `json:"sip_to_user"`
	DurationMS       int    `json:"duration_msec"`
	BillableMS       int    `json:"billable_msec"`
	SipCallID        string `json:"sip_call_id"`
	FeatureFlags     string `json:"feature_flags"`
	ReferredBy       string `json:"referred_by"`
	ReferTo          string `json:"refer_to"`
	DirectionA       string `json:"direction_a"`
	DirectionB       string `json:"direction_b"`
	ForwardedBy      string `json:"forwarded_by"`
	ForwardedByExten string `json:"forwarded_by_exten"`
	SrcLine          string `json:"src_line"`
	SrcExten         string `json:"src_exten"`
	DstLine          string `json:"dst_line"`
	DstExten         string `json:"dst_exten"`
	OrigDestNum      string `json:"orig_dest_num"`
	OrigCallerNum    string `json:"orig_caller_num"`
	GreetingPhase    string `json:"greeting_phase"`
	PrimaryCustTag   string `json:"primary_cust_tag"`
	SecondaryCustTag string `json:"secondary_cust_tag"`
	CallQueue        string `json:"call_queue"`
	FaxDocTransPages string `json:"fax_document_transferred_pages"`
	sqlFields        []string
	sqlInserted      bool
	sqlError         error
}

func newCDRfromCSV(csvLine string) (*CDR, error) {
	csvLine = strings.Replace(csvLine, "\n", "", -1)
	csvLine = strings.Replace(csvLine, "\r", "", -1)
	csvVals := strings.Split(csvLine, ",")
	csvLen := len(csvVals)
	cdrMap := make(map[string]string)
	for a := 0; a < csvLen && a < len(fields); a++ {
		cdrMap[fields[a]] = csvVals[a]
	}
	cdr := &CDR{sqlFields: make([]string, 30)}
	data, err := json.Marshal(cdrMap)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, cdr)
	if err != nil {
		return nil, err
	}
	return cdr, nil
}
func newCDRfromJSON(jsonStr string) (*CDR, error) {
	cdr := &CDR{sqlFields: make([]string, 30)}
	err := json.Unmarshal([]byte(jsonStr), cdr)
	if err != nil {
		return nil, err
	}
	return cdr, nil
}
