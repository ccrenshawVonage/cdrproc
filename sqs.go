package main

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

/*
type sqsmsg struct {
	Records []struct {
		S3 struct {
			Bucket struct {
				Name string
			}
			Object struct {
				Key string
			}
		}
	}
}
*/

type sqscdr struct {
	accountID string
	trackID   string
	data      *string
}

var sqsQueueURL string
var sqsTimeout int64
var sqsClient *sqs.SQS

func initSQS(queueName string, timeout int64) error {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	sqsClient := sqs.New(sess)
	urlResult, err := sqsClient.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName: &queueName,
	})
	if err != nil {
		return err
	}
	sqsQueueURL = *urlResult.QueueUrl
	sqsTimeout = timeout
	return nil
}

func sqsGet() ([]*sqscdr, error) {
	if msgResult, err := sqsClient.ReceiveMessage(&sqs.ReceiveMessageInput{
		AttributeNames: []*string{
			aws.String(sqs.MessageSystemAttributeNameSentTimestamp),
		},
		MessageAttributeNames: []*string{
			aws.String(sqs.QueueAttributeNameAll),
		},
		QueueUrl:            aws.String(sqsQueueURL),
		MaxNumberOfMessages: aws.Int64(1),
		VisibilityTimeout:   aws.Int64(sqsTimeout),
	}); err != nil {
		return nil, err
	} else {
		sqsCdrs := make([]*sqscdr, len(msgResult.Messages))
		for _, msg := range msgResult.Messages {
			sqsCdr := &sqscdr{data: msg.Body}
			if accountID, accountIDok := msg.Attributes["account_id"]; accountIDok {
				sqsCdr.accountID = *accountID
			}
			if trackID, trackIDok := msg.Attributes["track_id"]; trackIDok {
				sqsCdr.trackID = *trackID
			}
			sqsCdrs = append(sqsCdrs, sqsCdr)
		}
		return sqsCdrs, nil
	}
}

/*
	try {
		event = new JSONObject(message).getJSONArray("Records").getJSONObject(0).getJSONObject("s3");
	}
	catch (JSONException e) {
		event = new JSONObject(message);
		if (event.getString("Event").equals("s3:TestEvent")) {
			logger.info("MessageID=[%s] Found Test Message From S3.", messageID);
			return true;
		}
	}
	String bucket = event.getJSONObject("bucket").getString("name");
	String fileName = event.getJSONObject("object").getString("key");
	logger.info(String.format("MessageID=[%s] Bucket name=%s File name=%s",messageID,bucket,fileName));
	BufferedReader reader = s3Retriever.getObject(bucket, fileName);
	if (reader != null) {
		final AtomicLong completed = new AtomicLong(0);
		final AtomicBoolean failed = new AtomicBoolean(false);
		final AtomicLong featCompleted = new AtomicLong(0);
		final AtomicBoolean featFailed = new AtomicBoolean(false);
		int totalRecords = 0;
		int totalFeatRecords = 0;
		CSVReader csvReader = new CSVReader(reader);
		while (true) {
			String[] record;
			try {
				record = csvReader.readNext();
			}
			catch (Exception e) {
				logger.error("Exception.",e);
				break;
			}
			if (record == null) break;
			try {
				Map<String, String> parsedRecord = rawCDRParser.processRecord(record);
				if (parsedRecord != null) {
					JSONObject parsedJson = new JSONObject(parsedRecord);
					totalRecords = totalRecords + 1;
					kinesisRecordProducer.putRecord(parsedJson.toString(), parsedRecord.get("track_id"), appConfig.getCdrKplStreamName(), new FutureCallback<UserRecordResult>(){
						@Override
						public void onSuccess(UserRecordResult result) {
							completed.getAndIncrement();
						}
						@Override
						public void onFailure(Throwable t) {
							if (t instanceof UserRecordFailedException) {
								Attempt last = Iterables.getLast(((UserRecordFailedException) t).getResult().getAttempts());
								logger.error(String.format("MessageID=[%s] Record failed to send to kinesis. Last Attempt Error Message: %s", messageID, last.getErrorMessage()));
							}
							else {
								logger.error(String.format("MessageID=[%s] Unexpected Exception. Failed to send record to kinesis. Error Message: %s", messageID, t.getMessage()));
							}
							failed.set(true);
						}
					});
					// adding call queue and call group events to feature kinesis
					if ((parsedRecord.get("feature_flags") != null && (parsedRecord.get("feature_flags").contains("callgroup") || parsedRecord.get("feature_flags").contains("vr|vm"))) ||
						(parsedRecord.get("service_ids") != null && parsedRecord.get("service_ids").contains("CQ") &&
						((parsedRecord.get("call_queue") != null && !parsedRecord.get("call_queue").equals("NULL")) ||
						(parsedRecord.get("uuid") != null && parsedRecord.get("track_id") != null && parsedRecord.get("uuid").equals(parsedRecord.get("track_id")))))) {
						totalFeatRecords++;
						kinesisRecordProducer.putRecord(parsedJson.toString(), parsedRecord.get("track_id"), appConfig.getCdrKplFeatStreamName(), new FutureCallback<UserRecordResult>() {
							@Override
							public void onSuccess(UserRecordResult result) {
								featCompleted.getAndIncrement();
							}
							@Override
							public void onFailure(Throwable t) {
								if (t instanceof UserRecordFailedException) {
									Attempt last = Iterables.getLast(((UserRecordFailedException) t).getResult().getAttempts());
									logger.error(String.format("MessageID=[%s] Record failed to send to feat kinesis. Last Attempt Error Message: %s", messageID, last.getErrorMessage()));
								} else {
									logger.error(String.format("MessageID=[%s] Unexpected Exception. Failed to send record to feat kinesis. Error Message: %s", messageID, t.getMessage()));
								}
								featFailed.set(true);
							}
						});
					}
				}
			}
			catch (Exception e) {
				logger.error("Exception.",e);
			}
		}
		csvReader.close();
		reader.close();
		while (completed.get() < totalRecords && failed.get() == false) {
			try {
				logger.info("Records sent: " + completed.get());
				Thread.sleep(1000);

*/
