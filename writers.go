package main

import (
	"database/sql"
)
type hdapCGhist struct {}
type hdapCQhist struct {}
type rawCDRdb   struct {}
type vReporter  struct {}

var hdapCGhistDB,  hdapCQhistDB  *sql.DB
var hdapCGprepSQL, hdapCQprepSQL []*sql.Stmt

func init() {
	var err error
	if hdapCGhistDB, err = sql.Open("", ""); err != nil {

	}
	if hdapCQhistDB, err = sql.Open("", ""); err != nil {
		
	}
}
func (w *hdapCGhist) writeBatch(css []*CDRSet) {
	if hdapCGprepSQL == nil {
		hdapCGprepSQL, _ = buildSQLPrepList(hdapCGhistDB, "", 0, 0)
	}
	
	cdrs := make([]*CDR, len(css)*5)
	for _, cs := range css {
		if true {	// If this CDRSet should be written to this database...
			if hdapCGprepSQL == nil {
				// Error - maybe cannot connect to database?
				// Write to a private SQS queue for later reprocessing
			} else {
				cdrs = append(cdrs, cs.cdrs...)
			}
		}
	}
}

func (w *hdapCQhist) writeBatch(css []*CDRSet) {
	cdrs := make([]*CDR, len(css)*5)
	for _, cs := range css {
		cdrs = append(cdrs, cs.cdrs...)
	}
}

func (w *rawCDRdb) writeBatch(css []*CDRSet) {
	cdrs := make([]*CDR, len(css)*5)
	for _, cs := range css {
		cdrs = append(cdrs, cs.cdrs...)
	}
}

func (w *vReporter) writeBatch(css []*CDRSet) {
	cdrs := make([]*CDR, len(css)*5)
	for _, cs := range css {
		cdrs = append(cdrs, cs.cdrs...)
	}
}

