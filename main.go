package main

import (
	"time"
)

// CDRSet keeps all the CDRs for one track_id
type CDRSet struct {
	trackID string
	cdrC    chan *sqscdr
	cdrs    []*CDR
}

type writer interface {
	writeBatch([]*CDRSet)
}

// Destinations
// HDAP call group history table (harbor)
// HDAP call queue history table (harbor)
// vReporter kinesis stream (CQ/CG only?) (who writes to this?)
// Raw CDR database (magic lambda?)
// where does mediation write to?

func main() {
	if sqsErr := initSQS("theQueue", 10); sqsErr != nil {
		return
	}
	maxDoneCDRSets   := 500  // If we get to this number of trackids waiting to get saved, save them (regardless of time interval).
	maxDoneTimeSec   := 5    // If we get to this wait interval, save however many trackids we have  (keeps us from holding them too long).
	maxCurrSuspendAt := 5000 // Stop taking on new trackids if our current count of active trackids hits this. Keeps us from running away on sqs.

	writers     := []writer{ &hdapCGhist{}, &hdapCQhist{}, &rawCDRdb{}, &vReporter{}}
	currCdrSets := make(map[string]*CDRSet)
	doneCdrSets := make([]*CDRSet, maxDoneCDRSets)
	doneChan    := make(chan *CDRSet, 50)

	for {
		select {
		case cdrSet := <-doneChan:
			doneCdrSets = append(doneCdrSets, cdrSet)	// Move the completed CDRSet from the curr map to the done list.
			delete(currCdrSets, cdrSet.trackID)

			if len(doneCdrSets) >= maxDoneCDRSets {		// Accumulate the done CDRSets. If we hit our batch size, send as a batch to storage.
				for _, writer := range writers {
					writer.writeBatch(doneCdrSets)
				}
				doneCdrSets = make([]*CDRSet, maxDoneCDRSets)
			}

		case <-time.After(time.Duration(maxDoneTimeSec)*time.Second):
			if len(doneCdrSets) >= maxDoneCDRSets {		// If we've waited too long to hit our batch size, go ahead and save whatever we've got.
				for _, writer := range writers {		// This makes sure that we have a cap on the latency of saving our completed CDRs.
					writer.writeBatch(doneCdrSets)
				}
				doneCdrSets = make([]*CDRSet, maxDoneCDRSets)
			}

		default:
			if sqsCdrs, err := sqsGet(); err != nil {
				for _, sqsCdr := range sqsCdrs {
					if cdrSet, cdrSetok := currCdrSets[sqsCdr.trackID]; !cdrSetok {
						// If we have too many active cdr sets already, throw this one back by not processing it.
						// If we do not start processing this CDR, we'll be skipping over it in the queue, but forgetting
						// about it here. That will allow us to continue to read more messages from SQS, hopefully finding 
						// the remaining CDRs that we need to get rid of the CDR sets we're already holding.
						// The CDRs we ignore will have their visibility restored after N seconds, so in N seconds we'll see
						// this skipped CDR again. Hopefully by then we'll have completed some sets and will have more room.

						// We have room, so save the new set / track_id.
						if len(currCdrSets) < maxCurrSuspendAt {
							cdrSet = &CDRSet{trackID: sqsCdr.trackID, cdrC: make(chan *sqscdr), cdrs: make([]*CDR, 5)}
							currCdrSets[sqsCdr.trackID] = cdrSet
							go worker(cdrSet, doneChan)
							currCdrSets[sqsCdr.trackID].cdrC <- sqsCdr
						}
					} else {
						currCdrSets[sqsCdr.trackID].cdrC <- sqsCdr
					}
				}
			}
		}
	}
}


