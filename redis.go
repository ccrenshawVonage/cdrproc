package main

// Cache the extension type. Since the extension type doesn't (hardly) ever change, this should dramatically
// minimize redis hits - and make things much more reliable since redis gets flaky when we hit it too hard.
// The only case where an extension type would change would be if they changed extension numbers around, which
// is very very rare. We have a safety check that will look at the CDRs and if they don't seem to match up to
// what we have cached, we'll refresh its entry from redis (should never happen).

var extntype = struct {
	CG string
} {
	"Call Group",
}

var acctExtnType map[string]string

func init() {
	acctExtnType = make(map[string]string)
}

func isCG(accountID int, extn string) (bool, error) {
	return checkCachedType(accountID, extn, extntype.CG)
}

func checkCachedType(accountID int, extn, extnType string) (bool, error) {
	key := fmt.Sprintf("%d/%s", accountID, extn)
	if cachedType, ok := acctExtnType[key]; ok {
		return cachedType == extnType
	}
	// Look up in redis
	extnKeyStr := "hdap:account:%s:ext:%s"
	svcKeyStr  := "hdap:account:%s:service:%s"

	extKey := fmt.Sprintf(extnKeyStr, accountID, extn)
	extMap, ok := getRedisMap(extKey)
	if !ok {
		return false, fmt.Errorf("account/extn key not found")
	}
	svcID, ok := extMap["account_service_id"]
	if !ok {
		return false, fmt.Errorf("account_service_id not found in account/extn map entry")
	}
	svcMap, ok := getRedisMap(fmt.Sprintf(svcKeyStr, accountID, svcID))
	if !ok {
		return false, fmt.Sprintf("account/service key not found")
	}
	planName, ok := svcMap["plan_name"]
	if !ok {
		return false, fmt.Sprintf("plan_name not found in service map")
	}
	return planName == extnType, nil
}

func getRedisMap(key string) map[string]string {
	return nil
}
