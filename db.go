package main

import (
	"fmt"
	"database/sql"
)

var db *sql.DB

func init() {
	// Initialize the DB connection
}

/*
This is a batch inserting mechanism. The idea is that it is much cheaper to insert one batch of 500 rows than to insert
one row 500 times (transactions, etc.).

Have to deal with the fact that out of the 500 (?) rows, one row might be bad, breaking the entire insertion of 500 rows.
So we start by trying the whole batch. If that fails, split in half and insert 250 and 250. One fails, one succeeds.
Split the 250 in half again, and insert 125/125. Continue this process until the bad row is found. The other 499 would have
been inserted (or fewer, if there were multiple bad CDRs).

Now for specifics: while we allow any size to be submitted (like 500), we first start by making sure we are starting with
powers of two. So if you submitted 512, great. But if you submitted 500, then start by breaking this into smaller initial
batches of 256, 128, xxx. Once we have the initial batch(es), try inserting. If it fails, divide in half and recursively 
descend and insert again.

To make all this really fast we have a set of N prepared statements: one to insert one row, one to insert two rows, one
to insert four rows, eight rows, sixteen rows, etc. So, based on the chunk size we'll select the correct statement for insert.
The prepared statements are in a list, where the index is the power of two for the chunk size. So index 2 is for inserting four,
index 3 is for inserting eight, etc. This lets us pretty easily get the correct statement given the batch size.
*/

func insertSQL(cdrs []*CDR, ss []*sql.Stmt) {
	// Rows are inserted in chunks of powers of two. So if we have 350 records, split into 256, 64, and ... (whatever).
	// Ideally we get a big number that is already a power of two (like 512). But if not, no problem.
	// Each power-of-two chunk is the root of a tree - try the whole thing, if insert fails, split in half and try again.
	length   := int16(len(cdrs))
	twosList := twosDigits(length)					// Returns [2, 64, 256], or whatever - adds up to the batch size, broken to pow2.
	cdrLists := make([][]*CDR, len(twosList))		// Might be [[2]cdrs, [64]cdrs, [256]cdrs] - you get it.
	cdrStart := int16(0)
	for _, len2 := range twosList {
		cdrLists  = append(cdrLists, cdrs[cdrStart:(cdrStart+len2)-1])
		cdrStart += len2
	}
	// Each one of these inserts is potentially the root of a tree that we must descend and split until we can insert as many as we can.
	// If all records are good, then the insert of the whole chunks succeeds. Otherwise, this function will recursively split and descend.
	for _, cdrList := range cdrLists {
		insertSQL2(cdrList, ss)
	}
}
func insertSQL2(cdrs []*CDR, ss []*sql.Stmt) error {
	// The length of the CDR list is a pow2 - find it's index value so we can use it to index into the PreparedStatment list.
	cdrSize := int16(len(cdrs))
	ssIndex := twosDigit(cdrSize)	// Will return a 2 fpr size of 4, a 3 for size of 8, 4 for 16, 5 for 32, etc.
	if ssIndex >= int16(len(ss)) {
		return fmt.Errorf("Prepared statement list does not have statement for slot %d (only %d) elems", ssIndex, len(ss))
	}
	// Within the CDR is a convenience field; the list of args that need to be passed to this CDR row for insert.
	// It's set up by the handler that also defined the SQL insert statement, so this generic function simply grabs those 
	// fields and puts them into the prepared statement.
	argsList := make([]string, len(cdrs)*10)
	for _, cdr := range cdrs {
		argsList = append(argsList, cdr.sqlFields...)
	}
	if result, err := ss[ssIndex].Exec(argsList); err != nil {
		// If we failed, and there's only one cdr we're trying to insert, then we're at the bottom of the tree, and this is the busted cdr.
		// Stop here (can't go deeper anyway!). Set the error on this cdr - we're done. The other branches will have inserted the other cdrs.
		if len(cdrs) == 1 {
			cdrs[0].sqlError    = err
			cdrs[0].sqlInserted = false
		} else {
			// Split in half, and try again. Yes the bad CDR is in one side or the other. But at least one side will succeed. Will continue
			// to descend on the side with the broken CDR, until we can insert all the other CDRs but that one (or more, maybe).
			insertSQL2(cdrs[:(len(cdrs)/2)-1], ss)
			insertSQL2(cdrs[len(cdrs)/2:], ss)
		}
	} else {
		// Wow! The whole chunk succeeded! Set the flag and return. This branch is done.
		for _, cdr := range cdrs {
			cdr.sqlInserted = true
		}
	}
	return nil
}

func twosDigits(num int16) []int16 {
	twosDigit := int16(32768/2)			// Start with 100000000000000
	twosList  := make([]int16, 16)
	for twosDigit > 0 {
		if num & twosDigit != 0 {
			twosList = append(twosList, twosDigit)
		}
		twosDigit >>= 1
	}
	return twosList
}
func twosDigit(num int16) int16 {
	twosDigit := int16(32768/2)			// Start with 100000000000000
	for pos := int16(14); twosDigit > 0; pos-- {
		if num & twosDigit != 0 {
			return pos
		}
		twosDigit >>= 1
	}
	return -1
}

func buildSQLPrepList(db *sql.DB, base string, cols, count int) ([]*sql.Stmt, error) {
	prepList := make([]*sql.Stmt)
	sqlList  := buildSQLList(base, cols, count)
	for _, sql := range sqlList {
		if stmt, err := db.Prepare(sql); err != nil {
			prepList = append(prepList, db.Prepare())
		} else {
			return nil, err
		}
	}
	return prepList, nil
}
func buildSQLList(base string, cols, count int) []string {
	sqlList := make([]string, count)
	rows := 1
	for a := 0; a < count; a++ {
		sql := buildSQL(string, cols, rows)
		sqlList = append(sqlList, sql)
		rows *= 2
	}
}
func buildSQL(base string, cols, rows int) string {
	return ""
}
