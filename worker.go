package main

import (
	"strings"
	"encoding/json"
)

func worker(cs *CDRSet, done chan *CDRSet) {
	timeout := time.After(time.Duration(5)*time.Second)
	for {
		select {
		case <-timeout:
			timeout = nil

		case sqsCdr := <-cs.cdrC:
			cdr := &CDR{}
			if err := json.Unmarshal([]byte(*sqsCdr.data), cdr); err != nil {
				// log it, and send to an error queue or something
			}
			if addCDR(cs, cdr) {
				done <- cs
				return
			}
		}
	}
}

func addCDR(cs *CDRSet, cdr *CDR) bool {
	if strings.Contains(strings.ToLower(cdr.FeatureFlags), "callgroup") {
		if cg, err := isCG(cdr.AccountID, cdr.DstExten); err != nil {
			
		} else {
			if cg {

			} else {

			}
		}
	} else if strings.Contains(strings.ToUpper(cdr.ServiceIDs), "CQ") {
		if cdr.CallQueue != "" || (cdr.TrackID == cdr.UUID && cdr.TrackID != "") {
			
		}
	} else if strings.Contains(strings.ToLower(cdr.FeatureFlags), "vr|vm") {
		
	}

	cs.cdrs = append(cs.cdrs, cdr)
	// Add CDR to the CDRSet.
	// Look at the status of the set. If the call is done, return true. Otherwise, return false.
	return false
}
